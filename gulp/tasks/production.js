'use strict';

var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('prod', function(cb) {

    cb = cb || function() {};

    // global.production = true;
    process.env.NODE_ENV = 'production';

    runSequence(['scss', 'js', 'fonts',  'images'], cb);

});