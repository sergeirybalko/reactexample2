import 'whatwg-fetch';
require('es6-promise').polyfill();
import React from 'react';
import { render } from 'react-dom';
import Container from './components/container';

const app = document.getElementById('app');

render(
    <Container/>,
    app
);