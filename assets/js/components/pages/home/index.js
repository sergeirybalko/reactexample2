import React from 'react';
import FirstSection from './first-section';
import LatestNews from './latest-news';
import InsideCompany from './inside-company';
import HotVacancies from './hot-vacancies';
import Newsletter from './news-letter';

const Home = () => {
    return(
        <div className="home-page">
            <FirstSection/>
            <LatestNews/>
            <InsideCompany/>
            <HotVacancies/>
            <Newsletter/>
        </div>
    );
};

export default Home;

