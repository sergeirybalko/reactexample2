import React from 'react';

const FirstSection = () => {
    return(
        <section className="first-section" style={{backgroundImage: 'url(./dist/images/jpg/first-screen-bg.jpg)'}}>
            <div className="section-desc">
                <h1>Stu Unger Rise And Fall Of A Poker Genius</h1>
                <p>V7 Digital Photo Printing</p>
            </div>
            <div className="bottom-info">
                <div className="item-info">
                    <div className="item-info-img" style={{backgroundImage: 'url(./dist/images/svg/clock.svg)'}}></div>
                    <div className="item-info-txt">
                        <h6>The Skinny On Lcd Monitors</h6>
                    </div>
                </div>
                <div className="item-info">
                    <div className="item-info-img" style={{backgroundImage: 'url(./dist/images/svg/star.svg)'}}></div>
                    <div className="item-info-txt">
                        <h6>Guidelines For Inkjet Cartridge Refill</h6>
                    </div>
                </div>
                <div className="item-info">
                    <div className="item-info-img" style={{backgroundImage: 'url(./dist/images/svg/music-player.svg)'}}></div>
                    <div className="item-info-txt">
                        <h6>Do A Sporting Stag Do In Birmingham</h6>
                    </div>
                </div>
                <div className="item-info">
                    <div className="item-info-img" style={{backgroundImage: 'url(./dist/images/svg/house.svg)'}}></div>
                    <div className="item-info-txt">
                        <h6>Your Computer Usage</h6>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default FirstSection;
