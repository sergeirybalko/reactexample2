import React, { Component } from 'react';
import Masonry from 'react-masonry-component';

export default class InsideCompany extends Component{
    state = {
        images: []
    };
    componentWillMount(){
        let { images } = this.state,
            _this = this;
        fetch(`https://jsonplaceholder.typicode.com/photos?_start=1&_end=19`)
            .then(function(response) {
                return response.json()
            }).then(function(json) {
            images.push(...json);
            _this.setState({ news: images });
        }).catch(function(ex) {
            console.log('parsing failed', ex);
        });
    }
    render(){
        const images = this.state.images.map((item, index) =>{
            return(
                <li className={[2,5,10,11].includes(index+1) ? "grid-item big" : "grid-item"}  key={item.id}>
                    <img src={item.url} alt={item.title}/>
                </li>
            )
        }),
        masonryOptions = {
            transitionDuration: 0
        };
        return(
          <section className="inside-company">
              <h2 className="text-center">inside company</h2>
              <div className="wrap-company-images" ref={(node) => {this.grid = node}}>
                  <Masonry
                      className={'grid no-bullet'}
                      elementType={'ul'}
                      options={masonryOptions}
                      disableImagesLoaded={false}
                      updateOnEachImageLoad={false}
                  >
                      {images}
                  </Masonry>
              </div>
          </section>
        );
    }
}