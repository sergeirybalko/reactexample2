import React, { Component } from 'react';

export default class LatestNews extends Component{
    state = {
      start: 0,
      end: 6,
      news: [],
      images: []
    };
    getNews(){
        let { start, end, news, images} = this.state,
              _this = this;
        fetch(`https://jsonplaceholder.typicode.com/posts?_start=${start}&_end=${end}`)
            .then(function(response) {
                return response.json()
            }).then(function(json) {
                if(json.length > 0){
                    news.push(...json);
                    start += json.length;
                    end += json.length;
                    for(let i = 0; i < json.length; i++){
                        images.push(`url(http://lorempixel.com/432/288/nature/${Math.floor((Math.random() * 8) + 1)})`)
                    }
                    _this.setState({news: news, start: start, end: end, images: images });
                }
        }).catch(function(ex) {
            console.log('parsing failed', ex);
        });
    }
    render(){
        const images = this.state.images,
              news = this.state.news.map((item, index) =>{
            return(
                <div className="news" key={item.id}>
                    <div className="news-img" style={{backgroundImage: images[index]}}></div>
                    <div className="news-content">
                        <h4 className="title-news">{item.title}</h4>
                        <p>{item.body}</p>
                        <a href="#" className="read-news">Read me</a>
                    </div>
                </div>
            )
        });
        return(
          <section className="latest-news">
              <div className="row">
                  <div className="small-12 column">
                      <h2 className="text-center">LATEST NEWS</h2>
                      <div className="wrap-last-news">
                          {news}
                      </div>
                  </div>
              </div>
              <div className="more-news text-center">
                  <button className="button hollow secondary" onClick={::this.getNews}>More news</button>
              </div>
          </section>
        );
    }
}
