import React from 'react';

const HotVacancies = () => {
    return(
        <section className="hot-vacancies">
            <div className="row">
                <div className="small-12 column">
                    <h2 className="text-center">hot vacancies</h2>
                    <div className="grid-vacancies">
                        <div className="item-vacancy">
                            <div className="item-vacancy-img" style={{backgroundImage: 'url(./dist/images/svg/settings.svg)'}}></div>
                            <div className="item-vacancy-content">
                                <p className="small-title">A situation</p>
                                <h5>As soon as Computerized Tomography</h5>
                                <p className="desc-vacancy">No image is so essential</p>
                            </div>
                        </div>
                        <div className="item-vacancy">
                            <div className="item-vacancy-img" style={{backgroundImage: 'url(./dist/images/svg/avatar.svg)'}}></div>
                            <div className="item-vacancy-content">
                                <p className="small-title">A situation</p>
                                <h5>This is for the reason</h5>
                                <p className="desc-vacancy">No image is so essential</p>
                            </div>
                        </div>
                        <div className="item-vacancy">
                            <div className="item-vacancy-img" style={{backgroundImage: 'url(./dist/images/svg/phone-call.svg)'}}></div>
                            <div className="item-vacancy-content">
                                <p className="small-title">A situation</p>
                                <h5>became accessible in the 1970s</h5>
                                <p className="desc-vacancy">No image is so essential</p>
                            </div>
                        </div>
                        <div className="item-vacancy">
                            <div className="item-vacancy-img" style={{backgroundImage: 'url(./dist/images/svg/placeholder.svg)'}}></div>
                            <div className="item-vacancy-content">
                                <p className="small-title">A situation</p>
                                <h5>Magnetic Resonance Imaging </h5>
                                <p className="desc-vacancy">No image is so essential</p>
                            </div>
                        </div>
                        <div className="item-vacancy">
                            <div className="item-vacancy-img" style={{backgroundImage: 'url(./dist/images/svg/like.svg)'}}></div>
                            <div className="item-vacancy-content">
                                <p className="small-title">A situation</p>
                                <h5>After that, in the 1980s Magnetic</h5>
                                <p className="desc-vacancy">No image is so essential</p>
                            </div>
                        </div>
                        <div className="item-vacancy">
                            <div className="item-vacancy-img" style={{backgroundImage: 'url(./dist/images/svg/envelope.svg)'}}></div>
                            <div className="item-vacancy-content">
                                <p className="small-title">A situation</p>
                                <h5>MRIs concentrate on water molecules</h5>
                                <p className="desc-vacancy">No image is so essential</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="more-vacancy text-center">
                <button className="button hollow secondary">More vacancies</button>
            </div>
        </section>
    );
};

export default HotVacancies;
