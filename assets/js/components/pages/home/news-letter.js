import React, { Component } from 'react';

const Newsletter = () => {
    let email = '',
        response = '';
    const submitForm = (e) => {
        e.preventDefault();
        response.classList.remove('error');
        response.classList.remove('complete');
        const validations ={
            email: [/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/, 'Please enter a valid email address', 'Complete']
        };
        let validation = new RegExp(validations['email'][0]);
        if (!validation.test(email.value)){
            // If the validation fails then we show the custom error message
            response.classList.toggle('error');
            response.innerHTML = validations['email'][1];
        } else {
            // This is really important. If the validation is successful you need to reset the custom error message
           response.classList.toggle('complete');
           response.innerHTML = validations['email'][2];
            setTimeout(()=>{
                response.classList.remove('complete');
                response.innerHTML = '';
                email.value = '';
            }, 4000);
        }
    };
    return(
        <section className="newsletter" style={{backgroundImage: 'url(./dist/images/jpg/form-bg.jpg)'}}>
            <h1 className="text-center">Sign up for newsleter!</h1>
            <div className="row align-center">
                <div className="small-9 medium-6 large-5 xlarge-4 column">
                    <p>Stay informed of the last company news</p>
                    <form className="form-letter" onSubmit={submitForm}>
                        <div className="wrap-input">
                            <input type="email" placeholder="Your email" ref={node => email = node}/>
                            <button className="button">subscribe</button>
                            <p className="response" ref={node => response = node}></p>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    );
};

export default Newsletter;