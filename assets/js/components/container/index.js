import React from 'react';
import Header from '../header';
import Home from '../pages/home';

const Container = () => {
    return(
        <div className="container">
            <Header/>
            <Home/>
        </div>
    )
};

export default Container;

