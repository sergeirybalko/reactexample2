import React from 'react';

const Header = () => {
    let navBtn = '',
        wrapHeader = '';

    const navBtnClick = () => {
        navBtn.classList.toggle('active');
        wrapHeader.classList.toggle('active');
    };

    return(
        <header className="main-header">
            <div className="mobile-header">
                <a href="#" className="mobile-logo">
                    <svg version="1.1" x="0px" y="0px" viewBox="0 0 496 496" style={{enableBackground: 'new 0 0 496 496'}}>
                        <polygon style={{fill: '#FFC239'}} points="248,244 0,149.6 248,68.8 496,149.6 "/>
                        <polyline style={{fill: '#EFA91B'}} points="248,68.8 496,149.6 248,244 "/>
                        <polygon style={{fill: '#23C181'}} points="248,292.8 60.8,221.6 0,241.6 248,336 496,241.6 435.2,221.6 "/>
                        <polyline style={{fill: '#05966C'}} points="248,336 496,241.6 435.2,221.6 248,292.8 "/>
                        <polygon style={{fill: '#F44D3B'}} points="248,384.8 60.8,313.6 0,333.6 248,427.2 496,333.6 435.2,313.6 "/>
                        <polyline style={{fill: '#C42014'}} points="248,427.2 496,333.6 435.2,313.6 248,384.8 "/>
                    </svg>
                </a>
                <button className="nav-btn" onClick={navBtnClick} ref={node => navBtn = node}>
                    <span></span>
                </button>
            </div>
            <div className="wrap-header-content"  ref={node => wrapHeader = node}>
                <div className="row align-center">
                    <div className="small-12 large-8 column">
                        <div className="left-header">
                            <a href="#" className="desktop-logo">
                                <svg version="1.1" x="0px" y="0px" viewBox="0 0 496 496" style={{enableBackground: 'new 0 0 496 496'}}>
                                    <polygon style={{fill: '#FFC239'}} points="248,244 0,149.6 248,68.8 496,149.6 "/>
                                    <polyline style={{fill: '#EFA91B'}} points="248,68.8 496,149.6 248,244 "/>
                                    <polygon style={{fill: '#23C181'}} points="248,292.8 60.8,221.6 0,241.6 248,336 496,241.6 435.2,221.6 "/>
                                    <polyline style={{fill: '#05966C'}} points="248,336 496,241.6 435.2,221.6 248,292.8 "/>
                                    <polygon style={{fill: '#F44D3B'}} points="248,384.8 60.8,313.6 0,333.6 248,427.2 496,333.6 435.2,313.6 "/>
                                    <polyline style={{fill: '#C42014'}} points="248,427.2 496,333.6 435.2,313.6 248,384.8 "/>
                                </svg>
                            </a>
                            <ul className="main-menu no-bullet">
                                <li><a href="#">about us</a></li>
                                <li><a href="#">company</a></li>
                                <li><a href="#">careers</a></li>
                                <li><a href="#">contacts</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="small-12 large-4 column">
                        <div className="right-header">
                            <a href="#" className="button hollow">log in</a>
                            <a href="#" className="button">sign up</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
};

export default Header;

